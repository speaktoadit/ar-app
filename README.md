# ar-app
To run this project: 
 1. download the zip file and unzip it 
 2. host the entire file on an https server 
 3. access the files through an ARCore capable Android phone running Android 8
 4. download ARCore and Google Canary on phone
 5. go to chrome://flags your Google Canary browser 
 6. enable the following two flags: #webxr and #webxr-hit-test
 7. go to your hosted website on your phone
 8. you can also use the app on computer but all features will not be available 
 
This project used the the starter code provided in the artile written here: https://codelabs.developers.google.com/codelabs/ar-with-webxr/#1
My version of the project can be found at https://speaktoadit.com/ar-app

